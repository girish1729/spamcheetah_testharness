
import sys
import imaplib
import getpass
import email
import email.header
import datetime


EMAIL_ACCOUNT = "girish@gayatri-hitech.com"

# Use 'INBOX' to read inbox.  Note that whatever folder is specified, 
# after successfully running this script all emails in that folder 
# will be marked as read.
EMAIL_FOLDER = "INBOX"
EMAIL_FOLDER = "[Gmail]Sent Mail"
EMAIL_FOLDER = "Sent"
EMAIL_FOLDER = "Archive"


def process_mailbox(M):
    """
    Do something with emails messages in the folder.  
    For the sake of this example, print some headers.
    """

    rv, data = M.search(None, "ALL")
    if rv != 'OK':
        print("No messages found!")
        return

    for num in data[0].split():
        rv, data = M.fetch(num, '(RFC822)')
        if rv != 'OK':
            print("ERROR getting message", num)
            return

        msg = email.message_from_bytes(data[0][1])
        sub = email.header.make_header(email.header.decode_header(msg['Subject']),
'UTF-8')
        fromaddr = email.header.make_header(email.header.decode_header(msg['From']),
'UTF-8')
        toaddr = email.header.make_header(email.header.decode_header(msg['To']), 'UTF-8')
        subject = str(sub)
        n = num.decode()
        print('Message:(%s) %s' % (n, subject))
        print('From: %s' % (fromaddr))
        print('To: %s' % (toaddr))
        #print('Raw Date:', msg['Date'])
        date_tuple = email.utils.parsedate_tz(msg['Date'])
        if date_tuple:
            local_date = datetime.datetime.fromtimestamp(
                email.utils.mktime_tz(date_tuple))
            print ("Local Date:", \
                local_date.strftime("%a, %d %b %Y %H:%M:%S"))
        print("\n")

M = imaplib.IMAP4_SSL('imap.gmail.com')
try:
    rv, data = M.login(EMAIL_ACCOUNT, 'pant1729')
except imaplib.IMAP4.error:
    print ("LOGIN FAILED!!! ")
    sys.exit(1)

print(rv, data)

rv, mailboxes = M.list()
if rv == 'OK':
    print("Mailboxes:")
    for mbox in mailboxes:
     print(mbox, "\n")

rv, data = M.select(EMAIL_FOLDER)
if rv == 'OK':
    print("Processing mailbox...\n")
    process_mailbox(M)
    M.close()
else:
    print("ERROR: Unable to open mailbox ", rv)

M.logout()
