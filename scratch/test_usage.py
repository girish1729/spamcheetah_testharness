#!/usr/bin/env python
import psutil
# gives a single float value
cpuper = psutil.cpu_percent()
# you can convert that object to a dictionary 
#vmper = dict(psutil.virtual_memory()._asdict())
# you can have the percentage of used RAM
ramper = psutil.virtual_memory().percent
# you can calculate percentage of available memory
memavail = psutil.virtual_memory().available * 100 / psutil.virtual_memory().total
print("CPU usage \tMemory usage \tAvailable mem")
print(cpuper, ramper, memavail)
