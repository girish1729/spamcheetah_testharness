import smtplib 
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 
import os

fromaddr = "girish@gayatri-hitech.com"
toaddr = "girish@spamcheetah.hopto.org"
sub = "Test harness with attachment"
body = "Body_of_the_mail"
attach = "/home/girish/Pictures/Bhavna_churidhar.jpg"
attachment = open(attach, "rb") 
filename = os.path.basename(attach)

def prepare_mail():

  msg = MIMEMultipart() 
  
  msg['From'] = fromaddr 
  msg['To'] = toaddr 
  msg['Subject'] = sub
  msg.attach(MIMEText(body, 'plain')) 
  
  p = MIMEBase('application', 'octet-stream') 
  
  p.set_payload((attachment).read()) 
  encoders.encode_base64(p) 
  p.add_header('Content-Disposition', "attachment; filename= %s" %
  filename) 
  msg.attach(p) 
  return(msg)

def despatch(msg):
  s = smtplib.SMTP('smtp.gmail.com', 587) 
  s.starttls() 
  s.login(fromaddr, "pant1729") 
  text = msg.as_string() 
  s.sendmail(fromaddr, toaddr, text) 
  s.quit() 
  
msg = prepare_mail()
despatch(msg)
