from email.parser import Parser
parser = Parser()

emailText = """PUT THE RAW TEXT OF YOUR EMAIL HERE"""
email = parser.parsestr(emailText)

print email.get('From')
print email.get('To')
print email.get('Subject')

#The body is trickier. Call email.is_multipart(). If that's false, you
#can get your body by calling email.get_payload(). However, if it's true,
#email.get_payload() will return a list of messages, so you'll have to
#call get_payload() on each of those.

if email.is_multipart():
    for part in email.get_payload():
        print part.get_payload()
else:
    print email.get_payload()
