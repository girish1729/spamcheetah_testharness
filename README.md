# README #

SpamCheetah uses pytest to setup everything

### What is this repository for? ###

* This is a tool to test the Email spam control product SpamCheetah
* Version 1.1

### How do I get set up? ###

* SpamCheetah must be tested with lot of backend mail sending and
* receiving
* SpamCheetah is written in C, perl and typescript Angular frontend
* Not too keen on frontend we want mails to flow always

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owned by girish@gayatri-hitech.com
* Copyright Gayatri hitech
