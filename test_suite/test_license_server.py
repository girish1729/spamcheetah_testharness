import json
import pytest
import requests
from string import ascii_lowercase
import sys, os
from random import choice
from truth.truth import AssertThat

########################################################
#	Test all Licensing server HTTP POST endpoints 
########################################################

def makereq( body):
  resp = requests.post("https://licensing.spamcheetah.com/cgi-bin/licheck",json=body)
  return(resp)

# XXX
#  activate
#		$customer_email = $inpLic{customer_email};
#		$customer_name = $inpLic{customer_name};
#		$product_key = $inpLic{product_key};
#		$product = $inpLic{product_name};
#  purchase_license
#		$customer_name = $inpLic{customer_name};
#		$customer_email = $inpLic{customer_email};
#		$payment_id = $inpLic{payment_id};
#		$days = $inpLic{days};
#		$lic_type = $inpLic{license_type};
#		$product = $inpLic{product_name};
#		$domains = $inpLic{domains};
#		$mailids = $inpLic{mailids};
#
#
#  deactivate
#		$customer_email = $inpLic{"customer_email"};
#		$product = $inpLic{product_name};
#  dump_all
#
#  check_license
#		$product_key = $inpLic{product_key};
#		$customer_email = $inpLic{"customer_email"};
#		$customer_name = $inpLic{"customer_name"};
#		$product = $inpLic{product_name};
#		$license_type = $inpLic{license_type};
# XXX


#######################################
# XXX 

# Arrange
# Act
# Assert
# Cleanup
#def teardown_function():

def test_activate(capsys):
   req = {"verb":"activate",
	  "product_key": "trial",
	  "product": "SpamCheetah",
	  "customer_email": "girish@yahoo.com",
	  "customer_name": "Drink now"
   }
   resp = makereq(req)
   json = resp.json()
   assert json['code'] == 200
   print(resp.json())

def test_purchase_license(capsys):
   months = choice([1,3,6,12])
   max = choice([20,50,100,200,500,1000])
   type = choice(["standard", "heavyduty"])
   payid = ''.join(choice(ascii_uppercase) for i in range(8))
   custemail = choice([ 
	'girl@inda.ca', 
	'sat@inda.ca', 
	'fear@spamcheetah.ca',
	'tiger@dance.com', 
	'lion@singer.cl', 
	'sing@pineapple.in',
	'baz@inda.ca', 
	'abc@sing.in', 
	'boy@apple.ca'
    ])
   custname = choice([ 
	"Dance Boy", 
	"Mango Man",
	"Apple fool", 
	"Banana girl", 
	"Bhavna actress", 
	"Trisha beauty"
   ])
   req = {"verb":"purchase_license",
	  "customer_email": custemail,
	  "customer_name": custname,
	  "payment_id ": payid,
	  "months": months,
	  "license_type": type,
	  "product": "SpamCheetah",
	  "max": max
   }
   resp = makereq(req)
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_deactivate():
   email = choice(["girish@yahoo.in", "Shriya@gmail.com"])
   req = {"verb":"deactivate",
	  "product_name": "SpamCheetah",
	  "product_key": "SpamCheetah",
	  "customer_email": email
   }
   resp = makereq(req)
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_deactivate_wrongProd():
   email = choice(["girish@yahoo.in", "Shriya@gmail.com"])
   req = {"verb":"deactivate",
	  "product_name": "SpamCheetah",
	  "customer_email": email
   }
   resp = makereq(req)
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_deactivate_wrongCust():
   email = choice(["girish@gmail.in", "Shriya@hotmail.com"])
   req = {"verb":"deactivate",
	  "product_name": "SpamCheetah",
	  "customer_email": email
   }
   resp = makereq(req)
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200


def test_dump_all(capsys):
   req = {"verb":"dump_all" }
   resp = makereq(req)
   jsonout = resp.json()
   json = resp.json()
   print(json.dumps(jsonout, indent = 2))
   assert json['code'] == 200

def test_check_license(capsys):
   email = choice(["girish@yahoo.in", "Shriya@gmail.com"])
   license_type = choice(["standard", "heavyduty"])
   req = {"verb":"check_license",
		"product_key": "trial",
		"product_name": "SpamCheetah",
		"customer_name": "Mango Idiot",
		"license_type": license_type,
		"customer_email": "girish@yahoo.com"
   }
   resp = makereq(req)
   json = resp.json()
   print(resp.json())
   assert json['code'] == 200

def test_check_license_expired(capsys):
   req = {"verb":"check_license",
		"product_key": "trial",
		"product_name": "SpamCheetah",
	  	"product_key": "SpamCheetah",
		"customer_email": "girish@yahoo.com"
   }
   resp = makereq(req)
   json = resp.json()
   print(resp.json())
   assert json['code'] == 200


def test_check_license_wrongProduct(capsys):
   req = {"verb":"check_license",
		"product_key": "trial",
		"product_name": "Fool",
		"customer_email": "girish@yahoo.com"
   }
   resp = makereq(req)
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200


