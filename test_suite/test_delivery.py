import json
import pytest
import sys
from datetime import datetime
from truth.truth import AssertThat

import imaplib
import os
import email
import email.header
import mimetypes
from email.policy import default
from argparse import ArgumentParser
import smtplib
# For guessing MIME type based on file name extension
import mimetypes

from argparse import ArgumentParser

from email.message import EmailMessage
from email.policy import SMTP

from email.headerregistry import Address
from email.utils import make_msgid

###########################################################
# This is the Mail delivery test suite
###########################################################
def mail_dump_attach():
    parser = ArgumentParser(description="""\
Unpack a MIME message into a directory of files.
""")
    parser.add_argument('-d', '--directory', required=True,
                        help="""Unpack the MIME message into the named
                        directory, which will be created if it doesn't
already
                        exist.""")
    parser.add_argument('msgfile')
    args = parser.parse_args()

    with open(args.msgfile, 'rb') as fp:
        msg = email.message_from_binary_file(fp, policy=default)

    try:
        os.mkdir(args.directory)
    except FileExistsError:
        pass

    counter = 1
    for part in msg.walk():
        # multipart/* are just containers
        if part.get_content_maintype() == 'multipart':
            continue
        # Applications should really sanitize the given filename so that
        # an
        # email message can't be used to overwrite important files
        filename = part.get_filename()
        if not filename:
            ext = mimetypes.guess_extension(part.get_content_type())
            if not ext:
                # Use a generic bag-of-bits extension
                ext = '.bin'
            filename = f'part-{counter:03d}{ext}'
        counter += 1
        with open(os.path.join(args.directory, filename), 'wb') as fp:
            fp.write(part.get_payload(decode=True))

def send_mail_dircontents():
    parser = ArgumentParser(description="""\
Send the contents of a directory as a MIME message.
Unless the -o option is given, the email is sent by forwarding to your
local
SMTP server, which then does the normal delivery process.  Your local
machine
must be running an SMTP server.
""")
    parser.add_argument('-d', '--directory',
                        help="""Mail the contents of the specified
directory,
                        otherwise use the current directory.  Only the
regular
                        files in the directory are sent, and we don't
recurse to
                        subdirectories.""")
    parser.add_argument('-o', '--output',
                        metavar='FILE',
                        help="""Print the composed message to FILE
instead of
                        sending the message to the SMTP server.""")
    parser.add_argument('-s', '--sender', required=True,
                        help='The value of the From: header (required)')
    parser.add_argument('-r', '--recipient', required=True,
                        action='append', metavar='RECIPIENT',
                        default=[], dest='recipients',
                        help='A To: header value (at least one required)')
    args = parser.parse_args()
    directory = args.directory
    if not directory:
        directory = '.'
    # Create the message
    msg = EmailMessage()
    msg['Subject'] = f'Contents of directory {os.path.abspath(directory)}'
    msg['To'] = ', '.join(args.recipients)
    msg['From'] = args.sender
    msg.preamble = 'You will not see this in a MIME-aware mail reader.\n'

    for filename in os.listdir(directory):
        path = os.path.join(directory, filename)
        if not os.path.isfile(path):
            continue
        # Guess the content type based on the file's extension.
        # Encoding
        # will be ignored, although we should check for simple things
        # like
        # gzip'd or compressed files.
        ctype, encoding = mimetypes.guess_type(path)
        if ctype is None or encoding is not None:
            # No guess could be made, or the file is encoded
            # (compressed), so
            # use a generic bag-of-bits type.
            ctype = 'application/octet-stream'
        maintype, subtype = ctype.split('/', 1)
        with open(path, 'rb') as fp:
            msg.add_attachment(fp.read(),
                               maintype=maintype,
                               subtype=subtype,
                               filename=filename)
    # Now send or store the message
    if args.output:
        with open(args.output, 'wb') as fp:
            fp.write(msg.as_bytes(policy=SMTP))
    else:
        with smtplib.SMTP('localhost') as s:
            s.send_message(msg)

def html_send():
  # Create the base text message.
  msg = EmailMessage()
  msg['Subject'] = "Ayons asperges pour le déjeuner"
  msg['From'] = Address("Pepé Le Pew", "pepe", "example.com")
  msg['To'] = (Address("Penelope Pussycat", "penelope", "example.com"),
               Address("Fabrette Pussycat", "fabrette", "example.com"))
  msg.set_content("""\
  Salut!
  
  Hi <em> baby how are you? </em>
  
  --Girish
  """)
  
  # Add the html version.  This converts the message into a
  # multipart/alternative
  # container, with the original text message as the first part and the
  # new html
  # message as the second part.
  asparagus_cid = make_msgid()
  msg.add_alternative("""\
  <html>
    <head></head>
    <body>
      <p>Salut!</p>
      <p>Cela ressemble à un excellent
          <a
  href="http://www.yummly.com/recipe/Roasted-Asparagus-Epicurious-203718">
              recipie
          </a> déjeuner.
      </p>
      <img src="cid:{asparagus_cid}" />
    </body>
  </html>
  """.format(asparagus_cid=asparagus_cid[1:-1]), subtype='html')
  # note that we needed to peel the <> off the msgid for use in the html.
  
  # Now add the related image to the html part.
  with open("roasted-asparagus.jpg", 'rb') as img:
      msg.get_payload()[1].add_related(img.read(), 'image', 'jpeg',
                                       cid=asparagus_cid)
  
  # Make a local copy of what we are going to send.
  with open('outgoing.msg', 'wb') as f:
      f.write(bytes(msg))
  
  # Send the message via local SMTP server.
  with smtplib.SMTP('localhost') as s:
      s.send_message(msg)

def process_mailbox(M):
    """
    Do something with emails messages in the folder.  
    For the sake of this example, print some headers.
    """
    EMAIL_ACCOUNT = "girish@gayatri-hitech.com"
    
    # Use 'INBOX' to read inbox.  Note that whatever folder is specified, 
    # after successfully running this script all emails in that folder 
    # will be marked as read.
    EMAIL_FOLDER = "INBOX"
    EMAIL_FOLDER = "[Gmail]Sent Mail"
    EMAIL_FOLDER = "Sent"
    EMAIL_FOLDER = "Archive"


    rv, data = M.search(None, "ALL")
    if rv != 'OK':
        print("No messages found!")
        return

    for num in data[0].split():
        rv, data = M.fetch(num, '(RFC822)')
        if rv != 'OK':
            print("ERROR getting message", num)
            return

        msg = email.message_from_bytes(data[0][1])
        sub = email.header.make_header(email.header.decode_header(msg['Subject']),
'UTF-8')
        fromaddr = email.header.make_header(email.header.decode_header(msg['From']),
'UTF-8')
        toaddr = email.header.make_header(email.header.decode_header(msg['To']), 'UTF-8')
        subject = str(sub)
        n = num.decode()
        print('Message:(%s) %s' % (n, subject))
        print('From: %s' % (fromaddr))
        print('To: %s' % (toaddr))
        #print('Raw Date:', msg['Date'])
        date_tuple = email.utils.parsedate_tz(msg['Date'])
        if date_tuple:
            local_date = datetime.datetime.fromtimestamp(
                email.utils.mktime_tz(date_tuple))
            print ("Local Date:", \
                local_date.strftime("%a, %d %b %Y %H:%M:%S"))
        print("\n")
def read_mails():
  M = imaplib.IMAP4_SSL('imap.gmail.com')
  try:
      rv, data = M.login(EMAIL_ACCOUNT, 'pant1729')
  except imaplib.IMAP4.error:
      print ("LOGIN FAILED!!! ")
      sys.exit(1)
  
  print(rv, data)
  
  rv, mailboxes = M.list()
  if rv == 'OK':
      print("Mailboxes:")
      for mbox in mailboxes:
       print(mbox, "\n")
  
  rv, data = M.select(EMAIL_FOLDER)
  if rv == 'OK':
      print("Processing mailbox...\n")
      process_mailbox(M)
      M.close()
  else:
      print("ERROR: Unable to open mailbox ", rv)
  
  M.logout()

#XXX start
def test_maildeliver(capsys):
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_spamdeliver(capsys):
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_virusdeliver(capsys):
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_bannedattachdeliver(capsys):
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


