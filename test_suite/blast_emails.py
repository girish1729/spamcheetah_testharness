#!/usr/bin/env python3

import asyncio
import aiosmtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

MAIL_PARAMS = {
	'TLS': True, 
	'host': 'spamcheetah.com', 
	'password': 'mango',
	'user': 'girish', 
	'port': 587
}

fromid = "Bhavana Menon <bhavana@yahoo.com>"
toids = [
 "girish@test.spamcheetah.com",
 "email1@test.spamcheetah.com",
 "email2@test.spamcheetah.com",
 "email3@test.spamcheetah.com",
 "email4@test.spamcheetah.com",
 "email5@test.spamcheetah.com",
 "email6@test.spamcheetah.com"
]
counter=1

async def send_mail_async(sender, to, subject, text, textType='plain',
**params):
    cc = params.get("cc", [])
    bcc = params.get("bcc", [])
    mail_params = params.get("mail_params", MAIL_PARAMS)

    msg = MIMEMultipart()
    msg.preamble = subject
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = ', '.join(to)
    if len(cc): msg['Cc'] = ', '.join(cc)
    if len(bcc): msg['Bcc'] = ', '.join(bcc)

    msg.attach(MIMEText(text, textType, 'utf-8'))

    host = mail_params.get('host')
    isSSL = mail_params.get('SSL', False);
    isTLS = mail_params.get('TLS', False);
    port = mail_params.get('port', 465 if isSSL else 25)
    smtp = aiosmtplib.SMTP(hostname=host, port=port, use_tls=isSSL)
    print(f"sending...{to}\n")
    await smtp.connect()
    if isTLS:
        await smtp.starttls()
    if 'user' in mail_params:
        await smtp.login(mail_params['user'], mail_params['password'])
    await smtp.send_message(msg)
    await smtp.quit()

async def onemail(toid):
     global counter
     for _ in range(5):
       counter += 1
       output = await send_mail_async(fromid,
              [toid],
              f"Test {counter}",
              f'Test {counter} Message',
              textType="plain")

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    tasks = [asyncio.ensure_future(onemail(toid)) for toid in toids]
    loop.run_until_complete(asyncio.gather(*tasks))
    loop.close()
