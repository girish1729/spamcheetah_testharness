#!/usr/bin/env python3

import pytest
import asyncio
import aiosmtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from truth.truth import AssertThat


##################################################
# This is the Email load test suite
##################################################
MAIL_PARAMS = {
	'TLS': True, 
	'host': 'mail.spamcheetah.com', 
	'port': 587,
	'password': 'mango',
	'user': 'girish' 
}

fromid = "Bhavana Menon <girish@spamcheetah.com>"
toids = [
 "girish@mta.spamcheetah.com",
 "email1@mta.spamcheetah.com",
 "email2@mta.spamcheetah.com",
 "email3@mta.spamcheetah.com",
 "email4@mta.spamcheetah.com",
 "email5@mta.spamcheetah.com"
]
counter=1

async def send_mail_async(sender, to, subject,
**params):
    global counter
    cc = params.get("cc", [])
    bcc = params.get("bcc", [])
    mail_params = params.get("mail_params", MAIL_PARAMS)

    msg = MIMEMultipart()
    msg.preamble = subject
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = ', '.join(to)
    body = "Test body of the email\n envelope\nnow.\nGirish"
    if len(cc): msg['Cc'] = ', '.join(cc)
    if len(bcc): msg['Bcc'] = ', '.join(bcc)

    msg.attach(MIMEText(body, 'plain')) 

    host = mail_params.get('host')
    isSSL = mail_params.get('SSL', False);
    isTLS = mail_params.get('TLS', False);
    port = mail_params.get('port', 465 if isSSL else 25)
    smtp = aiosmtplib.SMTP(hostname=host, port=port, use_tls=isSSL)
    print(f"sending...{to}\n")
    await smtp.connect()
    if isTLS:
        await smtp.starttls()
    if 'user' in mail_params:
        await smtp.login(mail_params['user'], mail_params['password'])
    await smtp.send_message(msg)
    await smtp.quit()
    counter += 1

async def onemail(toid):
     global counter
     for _ in range(100):
       counter += 1
       output = await send_mail_async(fromid,
              [toid],
              f"mango idiot {counter}")

# XXX 
# Start
def test_pump_million(capsys):
    loop = asyncio.get_event_loop()
    tasks = [asyncio.ensure_future(onemail(toid)) for toid in toids]
    loop.run_until_complete(asyncio.gather(*tasks))
    loop.close()
    print(f"{counter} mails pumped")
