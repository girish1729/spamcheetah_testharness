import json
import pytest
import socket
import sys, os
from truth.truth import AssertThat

#######################################
#	Test all SOCKET requests
#######################################
# Arrange
# Act
# Assert
# Cleanup
#def teardown_function():

def recvall(sock):
        BUFF_SIZE = 4096
        data = b''
        while True:
         part = sock.recv(BUFF_SIZE)
         data += part
         if len(part) == 0:
         # either 0 or end of data
           break
        return data
def talk_to_socket(msg):
   sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
   broker_address = '/var/www/tmp/unixsock'
   proxy_address = '/var/www/tmp/proxysock'
   try:
       sock.connect(proxy_address)
   except socket.error as msg:
       print(msg)
       sys.exit(1)
   try:
       msg = bytes(msg, 'utf-8')
       sock.sendall(msg)
       data = recvall(sock)
   finally:
       sock.close()
   return(data.decode('utf8'))


#######################################
# XXX  Start tests
#######################################

# ****************************
# totalmailattemptcnt=0
# goodmailcnt=0
# numattcnt=0
# regexcnt=0
# viruscnt=0
# spamcnt=0
# bannednetcnt=0
# bannedsendercnt=0
# bannedrecipcnt=0
# blockedmimecnt=0
# rfcrejcnt=0
# fqdnrejcnt=0
# dkimrejcnt=0
# spfrejcnt=0
# rblrejcnt=0
# helorejcnt=0
# mailszcnt=0
# ****************************

def test_proxy_dumpstats():
   msg = "DUMPSTATS"
   resp = talk_to_socket(msg)
   print(resp)
   assert len(resp) > 0

def test_proxy_listdomains():
   msg = "LISTDOMAINS"
   resp = talk_to_socket(msg)
   print(resp)
   assert len(resp) > 0


def test_proxy_reset_counters():
   msg = "RESET_COUNTERS"
   resp = talk_to_socket(msg)
   print(resp)

def test_proxy_dump_usage_count():
   msg = "DUMP_USAGE_COUNTS"
   resp = talk_to_socket(msg)
   print(resp)
   assert len(resp) > 0

def test_proxy_dump_usage_mailid_list():
   msg = "DUMP_USAGE_LIST mailid"
   resp = talk_to_socket(msg)
   print(resp)
   assert len(resp) > 0

def test_proxy_dump_usage_domain_list():
   msg = "DUMP_USAGE_LIST domain"
   resp = talk_to_socket(msg)
   print(resp)
   assert len(resp) > 0


# attscan=true
# bannedattachact=PASS
# bannedattachsub=What is up
# bannedrecipsub=What is up
# bannedsendersub=What is up
# blockedmime=zip
# customer_email=mango@yahoo.in
# dkimcheck=true
# helocheck=false
# mailszexceedsub=What is up
# mtaip=192.46.228.41
# notificationflag=true
# product_key=trial
# quabody=What is up now 
#  Who is this 
#  then now
# quasub=Qua mail
# rblcheck=true
# regex_enable=true
# regex_pattern=man
# regex_type=HEADER
# reqfqdn=true
# rfccomp=true
# send_disclaimer=true
# spamact=REJECT
# spamfilt=true
# spfcheck=true
# stricthelocheck=true
# virusact=QUA
# virusrecipsub=What is up
# virusscan=false
# virussendersub=What is up
# virussendermail
# 	girish is an idiot
# What is up
# 
# virusrecipmail
# 	girish is an idiot
# What is up
# 
# quaexcept
# 	girish@india.com
# thrisha@yahoo.in
# 
# outdisclaimer
# 	this mail protected by
# marketed by us
# 
# mailszexceedmail
# 	girish is an idiot
# What is up
# 
# disclaimerexcept
# 	girish@india.com
# thrisha@yahoo.in
# 
# bannedsendermail
# 	girish is an idiot
# What is up
# 
# bannedsenderids
# 	mango@yahoo.in
# What@india.com
# 
# bannedrecipmail
# 	girish is an idiot
# What is up
# 
# bannedrecipids
# 	mango@yahoo.in
# india@man.in
# 
# bannedattachmail
# 	girish is an idiot
# What is up
# 
def test_proxy_dumpconfigvals():
   msg = "DUMPCONFIGVALS"
   resp = talk_to_socket(msg)
   print(resp)
   assert len(resp) > 0
