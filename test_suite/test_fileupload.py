import json
import pytest
import requests
import random
import sys
import psycopg2

  #url = 'http://httpbin.org/post'
  #r = requests.post(url, files=files)
###########################################################
# 		This is the File upload test suite        #
###########################################################

def fileupload(f, geturl, parms = {}):
  url = 'http://test.spamcheetah.com/cgi-bin/upld'
  files = {'fileupload': open(f, 'rb')}
  r = requests.post(url, files=files, auth=('access','dance'))
  print(r.text)
  PARAMS={"filename": f}
  PARAMS.update(parms)
  
  r = requests.get(geturl, params=PARAMS, auth=('access','dance'))
  return(r)

# XXX start

def test_feedspam():
  f = 'Bhavna_churidhar.jpg'
  geturl = 'http://test.spamcheetah.com/cgi-bin/feedspam'
  r = fileupload(f, geturl)
  print(r.text)

def test_upload_ourdomains(capsys):
  f="fool.txt"
  parms = {'action': 'ourdomains'}
  geturl = 'http://test.spamcheetah.com/cgi-bin/spamcheetah_config'
  r = fileupload(f, geturl, parms)
  print(r.text)


def test_upload_mtamap(capsys):
  f="fool.txt"
  parms = {'action': 'mtamap'}
  geturl = 'http://test.spamcheetah.com/cgi-bin/spamcheetah_config'
  r = fileupload(f, geturl, parms)
  print(r.text)
