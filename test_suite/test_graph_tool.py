import json
import pytest
import datetime as DT
import requests
import sys
from datetime import datetime
from truth.truth import AssertThat


########################################################
# This is the graph, tools in web interface test suite
########################################################
def getcharts(PARAMS):
  resp = requests.get("http://test.spamcheetah.com/cgi-bin/charts",
   params=PARAMS, auth=('access','dance'))
  return(resp)

def postTool(body):
  resp = requests.post("http://test.spamcheetah.com/cgi-bin/runTool",
   json=body, auth=('access','dance'))
  return(resp)


# Arrange
# Act
# Assert
# Cleanup
#def teardown_function():


# XXX Start
def test_live(capsys):
   resp = getcharts({"verb":"get"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_scatter(capsys):
   resp = getcharts({"type":"scatter"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_tablestats(capsys):
   today = int(DT.datetime.now().timestamp())
   week_ago = int(today - (7 * 86400))
   resp = getcharts({"type":"tablestats", "st": week_ago, "end": today})
   json = resp.json()
   print(resp.json())
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_linegraph(capsys):
   resp = getcharts({"type":"linegraph"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_pie(capsys):
   resp = getcharts({"type":"pie"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_bar(capsys):
   resp = getcharts({"type":"bar"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

# XXX now for the system commands

def test_ping(capsys):
   resp = postTool({"tool":"ping", "parms": {"hostname": "8.8.8.8"}})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_traceroute(capsys):
   resp = postTool({"tool":"traceroute", "parms": {"hostname": '8.8.8.8'}})
   print(resp.json())
   json = resp.json()
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_swaksmailid(capsys):
   resp = postTool({"tool":"swaksmailid", "fromid": "girish@yahoo.com",
	"mailid": "girish1729@gmail.com"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_showmailQ(capsys):
   resp = postTool({"tool":"showmailQ"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_systat(capsys):
   resp = postTool({"tool":"systat"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_pftop(capsys):
   resp = postTool({"tool":"pftop"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_smtpload(capsys):
   resp = postTool({"tool":"smtpload", "nummsg": 40, "msgsize": 1024})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

