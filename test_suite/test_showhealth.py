import psutil 
import pytest
import sys
from truth.truth import AssertThat
  
def test_showhealth(capsys):
  cpu = psutil.cpu_percent(1)
  mem = psutil.virtual_memory() 
  swap = psutil.swap_memory() 
  disk = psutil.disk_usage('/') 
  netio = psutil.net_io_counters()
  conn = psutil.net_connections()
  proc = psutil.process_iter()

  print(f"CPU :: {cpu} %")
  print(f"Memory usage :: {mem.percent} % ")
  print(f"Swap usage:: {swap.percent} %")
  print(f"Disk use:: {disk.percent} %")
  print(f"N/W Bytes sent:: {netio.bytes_sent} bytes")
  print(f"N/W Bytes recd:: {netio.bytes_recv} bytes")
  est = sum(1 for c in conn if c.status == 'ESTABLISHED')
  print(f"Live n/w connections :: {est}")
  est = sum(1 for ps in proc if ps.name() == 'smtprelay')
  memusage =[ps.memory_percent for ps in proc if ps.name() == 'smtprelay'] 
  memperc = sum(memusage)

  fduse =[ps.open_files() for ps in proc if ps.name() == 'smtprelay'] 
  print(f"SMTPrelay instances:: {est}")
  print(f"Memory usage of SMTPRelay:: {memperc}")
  for fd in fduse:
     print(fd)
  out,err = capsys.readouterr()
  sys.stdout.write(out)
  sys.stderr.write(err)
