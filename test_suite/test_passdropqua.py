import json
import pytest
import requests
import sys
from datetime import datetime
import psycopg2
from truth.truth import AssertThat

# XXX banattach PASS, VIRUS QUA, SPAM DROP
##################################################
# This is the PASS, REJECT, QUA test case
##################################################
# XXX Config
fromaddr = "girish@mtasender.cloudns.cl"
toaddr = "girish@test.spamcheetah.com"
attach = "Bhavna_churidhar.jpg"

# XXX sending
import smtplib 
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 
import os, hashlib
import mmap

# XXX receiving
import sys
from imapclient import IMAPClient
import hashlib
import mmap
from imap_tools import MailBox

def sha256sum(filename):
    h  = hashlib.sha256()
    with open(filename, 'rb') as f:
        with mmap.mmap(f.fileno(), 0, prot=mmap.PROT_READ) as mm:
            h.update(mm)
    return h.hexdigest()


# XXX sending
def prepare_mail(type):

  msg = MIMEMultipart() 
  
  msg['From'] = fromaddr 
  msg['To'] = toaddr 

  
  if type == 'SPAM':
     sub = "Test spam from Gtube"
     body = '''
Subject: Test spam mail (GTUBE)
Message-ID: <GTUBE1.1010101@example.net>
Date: Wed, 23 Jul 2003 23:30:00 +0200
From: Sender <sender@example.net>
To: Recipient <recipient@example.net>
Precedence: junk
MIME-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Content-Transfer-Encoding: 7bit

This is the GTUBE, the
	Generic
	Test for
	Unsolicited
	Bulk
	Email

If your spam filter supports it, the GTUBE provides a test by which you
can verify that the filter is installed correctly and is detecting
incoming
spam. You can send yourself a test mail containing the following string
of
characters (in upper case and with no white spaces and line breaks):

XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X

You should send this test mail from an account outside of your network.
This is the GTUBE, the
	Generic
	Test for
	Unsolicited
	Bulk
	Email

If your spam filter supports it, the GTUBE provides a test by which you
can verify that the filter is installed correctly and is detecting
incoming
spam. You can send yourself a test mail containing the following string
of
characters (in upper case and with no white spaces and line breaks):

XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X

You should send this test mail from an account outside of your network.
'''

     h = hashlib.sha256()
     h.update(body.encode('utf-8'))
     h = h.hexdigest()
     print("Sent mail with spam having hash ::", h)
  elif type == 'VIRUS':
     sub = "Test Eicar virus "
     body = """X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*"""
     h = hashlib.sha256()
     h.update(body.encode('utf-8'))
     h = h.hexdigest()
     print("Sent mail with virus having hash ::", h)
  elif type == 'BANATT':
     sub = "Test for banned attachment"
     body = 'Plain vanilla mail with attachment'
     p = MIMEBase('application', 'octet-stream') 
     attachment = open(attach, "rb") 
     filename = os.path.basename(attach)
     h= sha256sum(attach)
     p.set_payload((attachment).read()) 
     encoders.encode_base64(p) 
     p.add_header('Content-Disposition', "attachment; filename= %s" %
     filename) 
     msg.attach(p) 
     print("Sent mail with attachment having hash ::", h)

  msg['Subject'] = sub
  msg.attach(MIMEText(body, 'plain')) 
  return(msg, h)

def do_sending(type):
  msg, h = prepare_mail(type)
  despatch(msg)
  return(h)


def despatch(msg):
  s = smtplib.SMTP('mtasender.cloudns.cl', 587) 
  s = smtplib.SMTP('127.0.0.1', 587) 
  #s.starttls()
  #s.login(fromaddr, "panache") 
  text = msg.as_string() 
  s.sendmail(fromaddr, toaddr, text) 
  s.quit() 

# XXX receiving
def init_check_mail():

  server = IMAPClient('spamcheetah.zapto.org', use_uid=True)
  server.login('girish', 'panache1729')
  select_info = server.select_folder('INBOX')
  print('%d messages in INBOX' % select_info[b'EXISTS'])
  messages = server.search()
  print("%d mails " % len(messages))
  return(server, messages)
#messages = server.search(['FROM', 'best-friend@domain.com'])

def list_messages(server):
   messages = server.search()
   for msgid, data in server.fetch(messages, ['ENVELOPE']).items():
    envelope = data[b'ENVELOPE']
    print('ID #%d: "%s" received %s' % (msgid,
    envelope.subject.decode(), envelope.date))

def extract_attachments():
  with MailBox('spamcheetah.zapto.org').login(
         'girish', 'panache1729', 'INBOX') as mailbox:
    hash = None
    for message in mailbox.fetch():
        for att in message.attachments:  # list: [Attachment objects]
            with open("/tmp/" + att.filename, "wb") as f:
              f.write(bytearray(att.payload))
            f.close()
            hash = sha256sum(att.filename)
            os.unlink("/tmp/" + att.filename)
  return(hash)


def do_receiving():
 server, msg = init_check_mail()
 list_messages(server)
 h = extract_attachments()
 print("Got mail with attachment having hash ::", h)
 return(server,msg, h)
 
def del_mails(server, messages):
  server.delete_messages(messages)
  server.expunge()
  server.logout()

def getvals(stem, PARAMS):
  resp = requests.get("http://test.spamcheetah.com/cgi-bin/" + stem,
   params=PARAMS, auth=('access','dance'))
  return(resp)
###########################################################
# XXX start here
###########################################################


# Arrange
# Act
# Assert
# Cleanup
#def teardown_function():

@pytest.mark.skip()
def test_banattach_pass():
 hsend =  do_sending('BANATT')
 serv, msg, hrecv = do_receiving()
 assert hsend == hrecv
 # Cleanup
 del_mails(serv, msg)


def test_virus_qua():
 hsend =  do_sending('VIRUS')
 serv, msg, hrecv = do_receiving()
 assert hsend == hrecv
 # Cleanup
 del_mails(serv, msg)



def test_spam_drop():
 hsend =  do_sending('SPAM')
 serv, msg, hrecv = do_receiving()
 assert hsend == hrecv

