import json
import pytest
from random import choice
from string import ascii_lowercase
from truth.truth import AssertThat

from time import time
import hashlib
import sys
from datetime import datetime as dt
import psycopg2

###########################################################
# 		This is the Database test suite           #
###########################################################

def dbOpen():
  con = psycopg2.connect(database="postgres", user="postgres")
  #host="/var/www/tmp")
  print("Database opened successfully")
  return(con)

def queryDB(Q):
  cur = con.cursor()
  cur.execute(f'''{Q}''')
  rows = cur.fetchall()
  for row in rows:
      print(row)
  print("Table queried successfully")


def insertSQL(con, q):
  cur = con.cursor()
  Q = f'''{q}''' 
  cur.execute(Q)
  print(Q , " Successful")
  con.commit()

def updateTable(upd):
  cur = con.cursor()
  cur.execute(f"{upd}")
  con.commit()
  print("Total updated rows:", cur.rowcount)
  con.close()

def deleteTable(con, table):
  cur = con.cursor()
  cur.execute(f"DELETE from {table};")
  con.commit()
  print("Total deleted rows:", cur.rowcount)
  
def insmany(table, entries, cols):
  con = dbOpen()
  print(entries)
  with con:
      cur = con.cursor()
      cols = ",".join([str(s) for s in list(cols)])
      for en in entries:
        Q = f"INSERT INTO {table}({cols}) VALUES {en};" 
        cur.execute(Q)
      con.commit()
###########################################################
# XXX start here
###########################################################

@pytest.fixture(autouse=True)
def fixture_function():
     con = dbOpen()
     assert True
     yield
     deleteTable(con, "mails")
     deleteTable(con, "quamail")
     deleteTable(con, "quarantine_mailer")
     deleteTable(con, "license")
     deleteTable(con, "password_change_requests")
     
#\d mails
#   Column   |  Type   
#------------+---------
# id         | integer 
#nextval('mails_id_seq'
# envip      | text    
# envfrom    | text    
# envto      | text    
# fromid     | text    
# toid       | text    
# subject    | text    
# date       | text    
# headers    | text    
# size       | integer 
# timestamp  | integer 
# mailattr   | text    
# osfp       | text    
# helostring | text    
# mailqid    | text    
# msgid      | text    
#Indexes:
#    "mails_pkey" PRIMARY KEY, btree (id)

@pytest.repeat(100)
def test_mails_table(capsys):
   con = dbOpen()

   entries = []
   #for val in range(100000):
   for id in range(25,100):
      envip = "2.3.4.100" 
      envfrom = f"user{id}@yahoo.in"
      envto = f"girish{id}@gmail.in"
      fromid = f"user{id}@yahoo.in"
      toid = f"girish{id}@gmail.in"
      osfp = "Unknown"
      mailqid = f"xabc{id}df"
      helostring = ''.join(choice(ascii_lowercase) for i in range(12))
      msgid = f"somrandomjunk{id}@murugan"
      headers = "bunch of newlines\nhi\nhere\n"
      subject = f"Nice day {id}"
      date = dt.now().ctime()
      size = random.randint(100,100000)
      mailattr = 'mime:multipart'
      date = dt.now().ctime()
      ts = int(time())
      size = random.randint(100,100000)
      entries.append((envip,envfrom, envto, fromid, 
          toid, subject, date, headers, 
          ts, mailattr, osfp, helostring, mailqid))
   insmany('mails', entries, ['envip','envfrom','envto', 'fromid', 
	'toid', 'subject', 'date', 'headers', 
    'timestamp', 'mailattr', 'osfp', 'helostring', 'mailqid'])
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

#\d quamail
#  Column   |  Type   
#-----------+---------
# id        | integer 
#nextval('quamail_id_s
# mailfile  | text    
# headers   | text    
# size      | integer 
# subject   | text    
# date      | text    
# fromid    | text    
# toid      | text    
# timestamp | integer 
# mailattr  | text    
# envip     | text    
# reason    | text    
#Indexes:
#    "quamail_pkey" PRIMARY KEY, btree (id)
#


def test_qua_table(capsys):
   con = dbOpen()

   entries = []
   #for val in range(100000):
   for id in range(52,100):
      quafile = f"quafile.{id}"
      headers = "bunch of newlines\nhi\nhere\n"
      subject = f"Disco dancer {id}"
      date = dt.now().ctime()
      fromid = f"tuser{id}@yahoo.in"
      toid = f"bhavana{id}@gmail.in"
      ts =  int(time())
      mailattr = 'mime:multipart'
      envip = "100.20.4.30"
      quareason = random.choice(["spam","virus","banattach"])
      entries.append((quafile, headers, subject, date,
          fromid, toid, ts, mailattr, envip, quareason))
   insmany('quamail', entries, ['mailfile','headers','subject',
	 'date', 'fromid', 'toid', 'timestamp', 
         'mailattr', 'envip', 'reason'])
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

#\d quarantine_mailer
#   Column   |  Type   
#------------+---------
# tempid     | text    
# toid       | text    
# timewindow | integer 

def test_quamailer_table(capsys):
   con = dbOpen()

   entries = []
   #for val in range(100000):
   for id in range(20,80):
     id = 3
     user = f"mango{id}@yahoo.in"
     thash = hashlib.sha1(str(time()).encode()).hexdigest();
     entries.append((thash, user,int(dt.now().timestamp())))
   insmany('quarantine_mailer', entries, ['tempid','toid', 'timewindow'])
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

#\d license
#     Column     |  Type   
#----------------+---------
# id             | integer 
#nextval('license_id_seq'::
# customer_name  | text    
# customer_email | text    
# created        | integer 
# expires        | integer 
# payment_id     | text    
# license_type   | text    
# lic_secret     | text    
# lic_status     | text    
# product        | text    
# domains        | integer 
# mailids        | integer 


def test_license_table(capsys):
   con = dbOpen()

   entries = []
   #for val in range(100000):
   for id in range(20,80):
      customer_name = f"mango{id}"
      customer_email = f"dance{id}@gmail.com"
      days = random.choice([30,90,365])
      payment_id = "l0xadef0a"
      lic_type = "Trial"
      lic_secret = hashlib.sha1("mango".encode()).hexdigest();
      lic_status = "active"
      product = "SpamCheetah"
      domains = random.choice(range(20,1000))
      mailids = random.choice(range(50,800))
      entries.append((customer_name,customer_email,
	int(dt.now().timestamp()), days, payment_id,
	lic_type, lic_secret, lic_status, product, 
	domains, mailids))
   insmany('license', entries, ['customer_name','customer_email',
     'created', 'expires', 'payment_id', 'license_type',
     'lic_secret', 'lic_status', 'product', 'domains', 'mailids' ])
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

#\d password_change_requests
#   Column   |  Type   
#------------+---------
# tempid     | text    
# ident      | text    
# timewindow | integer 
#Indexes:
#    "password_change_requests_pkey" PRIMARY KEY, btree (tempid)
#Foreign-key constraints:
#    "password_change_requests_ident_fkey" FOREIGN KEY (ident) REFERENCES users(identifier)
#

def test_passchng_table(capsys):
   con = dbOpen()
   entries = []
   #for val in range(100000):
   for id in range(20,800):
      userid = f"girish"
      thash = hashlib.sha1(str(time()).encode()).hexdigest();
      entries.append((thash, userid,int(dt.now().timestamp())))
   insmany('password_change_requests', entries, ['tempid','ident',
       'timewindow'])
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)
