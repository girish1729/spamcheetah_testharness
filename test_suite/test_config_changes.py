import json
import pytest
import requests
import paramiko
import sys, os
from random import choice
from string import ascii_lowercase
from truth.truth import AssertThat

#######################################
#	Config file changes test cases
#######################################
ip='45.32.250.237'
port=22
username='girish'
password='panache'


def getreq(PARMS):
  resp = requests.get("http://test.spamcheetah.com/cgi-bin/spamcheetah_config",
   params=PARMS, auth=('access','dance'))
  return(resp)

def postreq(body):
  resp = requests.post("http://test.spamcheetah.com/cgi-bin/spamcheetah_config",
   json=body, auth=('access','dance'))
  return(resp)

# XXX NETWORK
@pytest.mark.repeat(2)
def test_get_network_config():
   confvals =  ["mtaip",
                "hostname",
                "usentp",
                "customer_name",
                "customer_email",
                "showutc",
                "ourtimezone",
                "submission",
                "static_routes",
                "iface_aliases"
	];
   #for val in range(100000):
   resp = postreq({"verb":"get", "parms": confvals})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
 
def test_set_network_config(capsys):
   payload = {
   "mtaip": "2.3.4.5",
   "hostname": "mango",
   "usentp": True,
   "showutc": True,
   "submission":True,
   "ourtimezone": "Asia/Kokata",
   "customer_name": "Mango dude ",
   "customer_email": "apple@girish.in",
   };
   print(payload);
   resp = postreq({"verb":"set", "cmd": "NET_CONFIG", "payload": payload})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_set_static_route(capsys):
   resp = getreq({"action":"static_route_add", "dest" : "20.30.4.5",
         		"gwy" : "45.32.250.1"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_set_iface_alias(capsys):
   resp = getreq({"action":"iface_alias_add", 
         "ipaddr" : "1.1.1.1",
         "netif" : "vio0",
         "mask" : "255.0.0.0"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_hostname(capsys):
   os.system("hostname")
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_ssl_enabling(capsys):
   os.system("nc -v localhost 443")
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)
 
def test_static_routes(capsys):
   os.system("netstat -rn ")
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)
 
def test_network_aliases(capsys):
   cmd='ifconfig -A' 
   ssh=paramiko.SSHClient()
   ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
   ssh.connect(ip,port,username,password)
   
   stdin,stdout,stderr=ssh.exec_command(cmd)
   outlines=stdout.readlines()
   resp=''.join(outlines)
   print(resp)

def test_timezone(capsys):
   os.system("date")
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_time_settings(capsys):
   os.system("pgrep ntpd")
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

# XXX MAIL_ACTIONS
#@pytest.mark.repeat(2)

def test_get_mail_actions():
   confvals =  ["virusscan",
                "attscan",
                "notifyuser",
                "notifyadmin",
                "virusupdateintvl",
                "virusact",
                "spamact",
                "bannedattachact",
	];
   resp = postreq({"verb":"get", "parms": confvals})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_set_mail_actions():
   virusact= choice(["QUA", "DROP", "PASS"])
   spamact= choice(["QUA", "DROP", "PASS"])
   bannedattachact= choice(["QUA", "DROP", "PASS"])
   payload = {
   "virusscan": False,
   "attscan": True,
   "notifyuser": False,
   "notifyadmin": True,
   "virusupdateintvl": 20,
   "virusact": virusact,
   "spamact": spamact,
   "bannedattachact": bannedattachact
   };
   resp = postreq({"verb":"set","cmd": "MAIL_ACTION_CONFIG", "payload": payload})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

# XXX PATTERN_MATCHING
def test_get_pattern_matching():
   confvals =  ["regex_enable",
                "regex_pattern",
                "regex_type",
                "blockedmime",
                "bannedrecipids",
                "bannedsenderids"
   ];
   resp = postreq({"verb":"get", "parms": confvals})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_set_pattern_matching():
   payload = {
   "regex_enable": True,
   "regex_pattern": "man",
   "regex_type": "HEADER",
   "blockedmime": [{"mime": "zip application/zip"}],
   "bannedrecipids": [{"mailid": "mango@yahoo.in"}, {"mailid":
"india@man.in"}],
   "bannedsenderids": [{"mailid": "mango@yahoo.in"}, {"mailid":
"What@india.com"}]
   };
   resp = postreq({"verb":"set","cmd": "REGEX_CONFIG", "payload": payload})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

# XXX QUARANTINE
@pytest.mark.repeat(2)
def test_get_quarantine():
   confvals = [ "quasub",
                "quabody",
                "quaexpiry",
                "quafreq",
                "quaexcept"
   ];
   resp = postreq({"verb":"get", "parms": confvals})
   json = resp.json()
   assert json['code'] == 200
   print(resp.json())

def test_set_quarantine():
   payload = {
    "quasub":"Qua mail",
   "quabody":"What is up now \n Who is this \n then now",
   "quaexpiry":30,
   "quafreq":100,
   "quaexcept":[ {"mailid": "girish@india.com"},
{"mailid":"thrisha@yahoo.in"}]
   };
   resp = postreq({"verb":"set", "cmd": "QUARANTINE_CONFIG", "payload": payload})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

# XXX MAIL_ENGINE
#@pytest.mark.repeat(3)
def test_get_mail_engine(capsys):
   confvals = [ "rfccomp",
                "reqfqdn",
                "rblcheck",
                "dkimcheck",
                "helocheck",
                "stricthelocheck",
                "spfcheck",
                "notificationflag",
                "bannedsendersub",
                "bannedsendermail",
                "bannedrecipsub",
                "bannedrecipmail",
                "bannedattachsub",
                "bannedattachmail",
                "virussendersub",
                "virussendermail",
                "virusrecipsub",
                "virusrecipmail",
                "mailszexceedsub",
                "mailszexceedmail",
                "send_disclaimer",
                "disclaimerexcept",
                "outdisclaimer"
   ];
   resp = postreq({"verb":"get", "parms": confvals})
   json = resp.json()
   print(resp.json())
   assert json['code'] == 200

def test_set_mail_engine():
   rfc = choice([True, False])
   reqfqdn= choice([True, False])
   rblcheck= choice([True, False])
   dkimcheck= choice([True, False])
   helocheck= choice([True, False])
   stricthelocheck= choice([True, False])
   spfcheck  = choice([True, False])
   disc  = choice([True, False])

   payload = {
   "rfccomp": rfc,
   "reqfqdn": reqfqdn,
   "rblcheck": rblcheck,
   "dkimcheck": dkimcheck,
   "helocheck": helocheck,
   "stricthelocheck": stricthelocheck,
   "spfcheck": spfcheck,
   "send_disclaimer": disc,
   "bannedsendersub":"What is up",
   "bannedsendermail":[ "girish is an idiot",
	"What is up"
   ],
   "bannedrecipsub":"What is up",
   "bannedrecipmail":[ "girish is an idiot",
	"What is up"
   ],
   "bannedattachsub":"What is up",
   "bannedattachmail":[ "girish is an idiot",
	"What is up"
   ],
   "virussendersub":"What is up",
   "virussendermail":[ "girish is an idiot",
	"What is up"
   ],
   "virusrecipsub":"What is up",
   "virusrecipmail":[ "girish is an idiot",
	"What is up"
   ],
   "mailszexceedsub":"What is up",
   "mailszexceedmail":[ "girish is an idiot",
	"What is up"
   ],
   "disclaimerexcept":[ {"mailid": "girish@india.com"}, {"mailid":
"thrisha@yahoo.in"}],
   "outdisclaimer":[ "this mail protected by", "marketed by us"]
   };
   resp = postreq({"verb":"set", "cmd": "MAIL_ENGINE_CONFIG", "payload": payload})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   

