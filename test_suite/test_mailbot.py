#!/usr/bin/python

import pytest
import time
from truth.truth import AssertThat
from random import choice

# XXX sending
import smtplib 
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 
import mmap

# XXX receiving
import sys
from imapclient import IMAPClient
import mmap
from imap_tools import MailBox

##################################################
# This is the mailbot autoresponder test
##################################################

# XXX mutt -F .muttrc -s "bannedsender add dance@fire.com"  mailbot@spamcheetah</dev/null
# XXX mutt -F .muttrc -s "bannedsender remove dance@fire.com"  mailbot@spamcheetah</dev/null
# XXX mutt -F .muttrc -s "bannedrecip add dance@fire.com"  mailbot@spamcheetah</dev/null
# XXX mutt -F .muttrc -s "bannedrecip remove dance@fire.com"  mailbot@spamcheetah</dev/null
# XXX mutt -F .muttrc -s "spamcontent"  mailbot@spamcheetah<spam.eml

# XXX mutt -F .muttrc -s "ping"  mailbot@spamcheetah</dev/null


# XXX Config
fromaddr = "girish@spamcheetah.com"
toaddr =  "mailbot@spamcheetah.my";
mailid = "fool@mango.com"
body = "Test of the email\n envelope\nnow.\nGirish"

def despatch(msg, to):
  s = smtplib.SMTP('test.spamcheetah.com', 587) 
  s.connect('test.spamcheetah.com', 587)
  s.ehlo()
  s.starttls()
  s.ehlo()
  s.login('qua', 'qua');
  text = msg.as_string() 
  s.sendmail(fromaddr, to, text) 
  s.quit() 


def prepare_mail(type):
  global body
  if type == "bansender_add":
  	sub = "bannedsender add " + mailid
  elif type == "bansender_remove":
  	sub = "bannedsender remove " + mailid
  elif type == "banrecip_add":
  	sub = "bannedrecip remove " + mailid
  elif type == "banrecip_remove":
  	sub = "bannedrecip remove " + mailid
  elif type == "ping":
  	sub = "ping"
  elif type == "spamcontent":
  	sub = "spamcontent"
  	with open("./spam.eml", encoding = 'utf-8') as f:
             body = f.read()

  msg = MIMEMultipart() 
  msg['From'] = fromaddr 
  msg['To'] = toaddr 
  msg['Subject'] = sub
  msg.attach(MIMEText(body, 'plain')) 
  return(msg,  toaddr)


# XXX receiving
def init_check_mail():
  #server = IMAPClient('mta.spamcheetah.com', use_uid=True)
  #erver.login('girish', 'foo')
  server = IMAPClient('spamcheetah.com', use_uid=True)
  server.login('girish', 'foo')
  select_info = server.select_folder('INBOX')
  print('%d messages in INBOX' % select_info[b'EXISTS'])
  messages = server.search()
  print("%d mails " % len(messages))
  return(server, messages)
#messages = server.search(['FROM', 'best-friend@domain.com'])

def list_messages(server):
   messages = server.search()
   for msgid, data in server.fetch(messages, ['ENVELOPE']).items():
    envelope = data[b'ENVELOPE']
    print('ID #%d: "%s" received %s' % (msgid,
    envelope.subject.decode(), envelope.date))
   return


def do_sending(type):
  msg, toaddr = prepare_mail(type)
  despatch(msg, toaddr )
  return

def do_receiving(type):
 server, msg = init_check_mail()
 list_messages(server)
 print("Got mailbot response")
 return(server,msg)
 
def del_mails(server, messages):
  ret = server.delete_messages(messages)
  print("Delete messages ::" , ret)
  server.expunge()
  server.logout()

# XXX mutt -F .muttrc -s "bannedsender add dance@fire.com"  mailbot@spamcheetah</dev/null
@pytest.mark.repeat(5)
def test_bansender_add(capsys):
 do_sending("bansender_add")
 return
 time.sleep(3)
 serv, msg = do_receiving()
 out,err = capsys.readouterr()
 sys.stdout.write(out)
 sys.stderr.write(err)
 # Cleanup
 del_mails(serv, msg)
 assert hsend == hrecv


# XXX mutt -F .muttrc -s "bannedsender remove dance@fire.com"  mailbot@spamcheetah</dev/null
@pytest.mark.repeat(5)
def test_bansender_remove(capsys):
 do_sending("bansender_remove")
 return
 time.sleep(3)
 serv, msg = do_receiving()
 out,err = capsys.readouterr()
 sys.stdout.write(out)
 sys.stderr.write(err)
 # Cleanup
 del_mails(serv, msg)
 assert hsend == hrecv


# XXX mutt -F .muttrc -s "bannedrecip add dance@fire.com"  mailbot@spamcheetah</dev/null
@pytest.mark.repeat(5)
def test_banrecip_add(capsys):
 do_sending("banrecip_add")
 return
 time.sleep(3)
 serv, msg = do_receiving()
 out,err = capsys.readouterr()
 sys.stdout.write(out)
 sys.stderr.write(err)
 # Cleanup
 del_mails(serv, msg)
 assert hsend == hrecv


# XXX mutt -F .muttrc -s "bannedrecip remove dance@fire.com"  mailbot@spamcheetah</dev/null
@pytest.mark.repeat(5)
def test_banrecip_remove(capsys):
 do_sending("banrecip_remove")
 return
 time.sleep(3)
 serv, msg = do_receiving()
 out,err = capsys.readouterr()
 sys.stdout.write(out)
 sys.stderr.write(err)
 # Cleanup
 del_mails(serv, msg)
 assert hsend == hrecv


# XXX mutt -F .muttrc -s "spamcontent"  mailbot@spamcheetah<spam.eml
@pytest.mark.repeat(5)
def test_banrecip_remove(capsys):
 do_sending("spamcontent")
 return
 time.sleep(3)
 serv, msg = do_receiving()
 out,err = capsys.readouterr()
 sys.stdout.write(out)
 sys.stderr.write(err)
 # Cleanup
 del_mails(serv, msg)
 assert hsend == hrecv

# XXX mutt -F .muttrc -s "ping"  mailbot@spamcheetah</dev/null
#@pytest.mark.repeat(5)
def test_ping(capsys):
 do_sending("ping")
 return
 time.sleep(3)
 serv, msg = do_receiving()
 out,err = capsys.readouterr()
 sys.stdout.write(out)
 sys.stderr.write(err)
 # Cleanup
 del_mails(serv, msg)
 assert hsend == hrecv


