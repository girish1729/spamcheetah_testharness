import json
import pytest
import socket
import sys, os
from truth.truth import AssertThat

# XXX sending
import smtplib 
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 
import os, hashlib
import mmap

# XXX receiving
import sys
from imapclient import IMAPClient
import hashlib
import mmap
from imap_tools import MailBox

#######################################
#	Test all Proxy counters
#######################################
# XXX Config
fromaddr = "girish@mtasender.cloudns.cl"
toaddr = "girish@test.spamcheetah.com"
attach = "Bhavna_churidhar.jpg"
def sha256sum(filename):
    h  = hashlib.sha256()
    with open(filename, 'rb') as f:
        with mmap.mmap(f.fileno(), 0, prot=mmap.PROT_READ) as mm:
            h.update(mm)
    return h.hexdigest()



# XXX sending
def prepare_mail(type):

  msg = MIMEMultipart() 
  
  msg['From'] = fromaddr 
  msg['To'] = toaddr 

  
  if type == 'SPAM':
     sub = "Test spam from Gtube"
     body = '''
Subject: Test spam mail (GTUBE)
Message-ID: <GTUBE1.1010101@example.net>
Date: Wed, 23 Jul 2003 23:30:00 +0200
From: Sender <sender@example.net>
To: Recipient <recipient@example.net>
Precedence: junk
MIME-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Content-Transfer-Encoding: 7bit

This is the GTUBE, the
	Generic
	Test for
	Unsolicited
	Bulk
	Email

If your spam filter supports it, the GTUBE provides a test by which you
can verify that the filter is installed correctly and is detecting
incoming
spam. You can send yourself a test mail containing the following string
of
characters (in upper case and with no white spaces and line breaks):

XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X

You should send this test mail from an account outside of your network.
This is the GTUBE, the
	Generic
	Test for
	Unsolicited
	Bulk
	Email

If your spam filter supports it, the GTUBE provides a test by which you
can verify that the filter is installed correctly and is detecting
incoming
spam. You can send yourself a test mail containing the following string
of
characters (in upper case and with no white spaces and line breaks):

XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X

You should send this test mail from an account outside of your network.
'''

     h = hashlib.sha256()
     h.update(body.encode('utf-8'))
     h = h.hexdigest()
     print("Sent mail with spam having hash ::", h)
  elif type == 'VIRUS':
     sub = "Test Eicar virus "
     body = """X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*"""
     h = hashlib.sha256()
     h.update(body.encode('utf-8'))
     h = h.hexdigest()
     print("Sent mail with virus having hash ::", h)
  elif type == 'BANATT':
     sub = "Test for banned attachment"
     body = 'Plain vanilla mail with attachment'
     p = MIMEBase('application', 'octet-stream') 
     attachment = open(attach, "rb") 
     filename = os.path.basename(attach)
     h= sha256sum(attach)
     p.set_payload((attachment).read()) 
     encoders.encode_base64(p) 
     p.add_header('Content-Disposition', "attachment; filename= %s" %
     filename) 
     msg.attach(p) 
     print("Sent mail with attachment having hash ::", h)

  msg['Subject'] = sub
  msg.attach(MIMEText(body, 'plain')) 
  return(msg, h)

def do_sending(type):
  msg, h = prepare_mail(type)
  despatch(msg)
  return(h)


def despatch(msg):
  s = smtplib.SMTP('mtasender.cloudns.cl', 587) 
  s = smtplib.SMTP('127.0.0.1', 587) 
  #s.starttls()
  #s.login(fromaddr, "panache") 
  text = msg.as_string() 
  s.sendmail(fromaddr, toaddr, text) 
  s.quit() 

# XXX receiving
def init_check_mail():

  server = IMAPClient('spamcheetah.zapto.org', use_uid=True)
  server.login('girish', 'panache1729')
  select_info = server.select_folder('INBOX')
  print('%d messages in INBOX' % select_info[b'EXISTS'])
  messages = server.search()
  print("%d mails " % len(messages))
  return(server, messages)
#messages = server.search(['FROM', 'best-friend@domain.com'])

def list_messages(server):
   messages = server.search()
   for msgid, data in server.fetch(messages, ['ENVELOPE']).items():
    envelope = data[b'ENVELOPE']
    print('ID #%d: "%s" received %s' % (msgid,
    envelope.subject.decode(), envelope.date))

def extract_attachments():
  with MailBox('spamcheetah.zapto.org').login(
         'girish', 'panache1729', 'INBOX') as mailbox:
    hash = None
    for message in mailbox.fetch():
        for att in message.attachments:  # list: [Attachment objects]
            with open("/tmp/" + att.filename, "wb") as f:
              f.write(bytearray(att.payload))
            f.close()
            hash = sha256sum(att.filename)
            os.unlink("/tmp/" + att.filename)
  return(hash)


def do_receiving():
 server, msg = init_check_mail()
 list_messages(server)
 h = extract_attachments()
 print("Got mail with attachment having hash ::", h)
 return(server,msg, h)
 
def del_mails(server, messages):
  server.delete_messages(messages)
  server.expunge()
  server.logout()

def getvals(stem, PARAMS):
  resp = requests.get("http://test.spamcheetah.com/cgi-bin/" + stem,
   params=PARAMS, auth=('access','dance'))
  return(resp)


# XXX sending
def talk_to_socket(msg):
   # Create a UDP socket
   sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
   
   # Connect the socket to the port where the server is listening
   broker_address = '/var/www/tmp/unixsock'
   proxy_address = '/var/www/tmp/proxysock'
   print('connecting to {}'.format(proxy_address))
   try:
       sock.connect(proxy_address)
   except socket.error as msg:
       print(msg)
       sys.exit(1)
   
   try:
       msg = bytes(msg, 'utf-8')
       sock.sendall(msg)
   
       data = sock.recv(1024)
       print('received {!r}'.format(data))
   finally:
       print('closing socket')
       sock.close()
   
   return(data)

#######################################
# XXX  Start tests
#######################################

def test_proxy_dumpstats(capsys):
 
# ****************************
# totalmailattemptcnt=0
# goodmailcnt=0
# numattcnt=0
# regexcnt=0
# viruscnt=0
# spamcnt=0
# bannednetcnt=0
# bannedsendercnt=0
# bannedrecipcnt=0
# blockedmimecnt=0
# rfcrejcnt=0
# fqdnrejcnt=0
# dkimrejcnt=0
# spfrejcnt=0
# rblrejcnt=0
# helorejcnt=0
# mailszcnt=0
# ****************************

   msg = "DUMPSTATS"
   resp = talk_to_socket(msg)
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_proxy_dumpconfigvals(capsys):

# attscan=true
# bannedattachact=PASS
# bannedattachsub=What is up
# bannedrecipsub=What is up
# bannedsendersub=What is up
# blockedmime=zip
# customer_email=mango@yahoo.in
# dkimcheck=true
# helocheck=false
# mailszexceedsub=What is up
# mtaip=192.46.228.41
# notificationflag=true
# product_key=trial
# quabody=What is up now 
#  Who is this 
#  then now
# quasub=Qua mail
# rblcheck=true
# regex_enable=true
# regex_pattern=man
# regex_type=HEADER
# reqfqdn=true
# rfccomp=true
# send_disclaimer=true
# spamact=REJECT
# spamfilt=true
# spfcheck=true
# stricthelocheck=true
# virusact=QUA
# virusrecipsub=What is up
# virusscan=false
# virussendersub=What is up
# virussendermail
# 	girish is an idiot
# What is up
# 
# virusrecipmail
# 	girish is an idiot
# What is up
# 
# quaexcept
# 	girish@india.com
# thrisha@yahoo.in
# 
# outdisclaimer
# 	this mail protected by
# marketed by us
# 
# mailszexceedmail
# 	girish is an idiot
# What is up
# 
# disclaimerexcept
# 	girish@india.com
# thrisha@yahoo.in
# 
# bannedsendermail
# 	girish is an idiot
# What is up
# 
# bannedsenderids
# 	mango@yahoo.in
# What@india.com
# 
# bannedrecipmail
# 	girish is an idiot
# What is up
# 
# bannedrecipids
# 	mango@yahoo.in
# india@man.in
# 
# bannedattachmail
# 	girish is an idiot
# What is up
# 
   msg = "DUMPCONFIGVALS"
   resp = talk_to_socket(msg)
   print(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_spamconf_bannedsender_add(capsys):
   resp = getvals({"verb":"bannedsender_del",
		"mailid": "bhavana@yahoo.com"})
   print(resp.json())
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_bannedsender_del(capsys):
   resp = getvals({"verb":"bannedsender_add",
		"mailid": "bhavana@yahoo.com"})
   print(resp.json())
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_bannedrecipient_del(capsys):
   resp = getvals({"verb":"bannedrecipient_del",
		"mailid": "bhavana@gmail.com"})
   print(resp.json())
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_bannedrecipient_add(capsys):
   resp = getvals({"verb":"bannedrecipient_add",
		"mailid": "bhavana@gmail.com"})
   print(resp.json())
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)



def test_mail_attach(capsys):
# numattcnt=0
   resp = send_mail()
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_regex_match(capsys):
# regexcnt=0
   resp = send_mail()
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_virus_send(capsys):
# viruscnt=0
   resp = send_mail()
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spam_send(capsys):
# spamcnt=0
   resp = send_mail()
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_bannedsender_send(capsys):
# bannedsendercnt=0
   resp = send_mail()
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_bannedrecip_send(capsys):
# bannedrecipcnt=0
   resp = send_mail()
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_bannedattach_send(capsys):
# blockedmimecnt=0
   resp = send_mail()
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_rfcreject_send(capsys):
# rfcrejcnt=0
   resp = send_mail()
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_fqdnreject_send(capsys):
# fqdnrejcnt=0
   resp = send_mail()
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_dkimreject_send(capsys):
# dkimrejcnt=0
   resp = send_mail()
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spfreject_send(capsys):
# spfrejcnt=0
   resp = send_mail()
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_rblreject_send(capsys):
# rblrejcnt=0
   resp = send_mail()
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_heloreject_send(capsys):
# helorejcnt=0
   resp = send_mail()
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_mailszreject_send(capsys):
# mailszcnt=0
   resp = send_mail()
   print(resp.text)
   assert(resp.text)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_banattach():
 hsend =  do_sending('BANATT')
 serv, msg, hrecv = do_receiving()
 assert hsend == hrecv
 # Cleanup
 del_mails(serv, msg)


def test_virus():
 hsend =  do_sending('VIRUS')
 serv, msg, hrecv = do_receiving()
 assert hsend == hrecv
 # Cleanup
 del_mails(serv, msg)


def test_spam():
 hsend =  do_sending('SPAM')
 serv, msg, hrecv = do_receiving()
 assert hsend == hrecv

