#!/usr/bin/python

import pytest
import time
from random import choice

# XXX sending
import smtplib 
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 
import os, hashlib
import mmap

# XXX receiving
import sys
from imapclient import IMAPClient
import hashlib
import mmap
from imap_tools import MailBox

##################################################
# This is the Simple mail sending/receiving test
##################################################

# XXX Config
fromaddr = "girish@spamcheetah.com"
toaddr = "girish@mta.spamcheetah.com"
to = [ "email1@mta.spamcheetah.com",
	"email2@mta.spamcheetah.com",
	"email3@mta.spamcheetah.com",
	"email4@mta.spamcheetah.com",
	"email5@mta.spamcheetah.com"
];
body = "Test body of the email\n envelope\nnow.\nGirish"
attach = os.path.dirname(__file__) + "/Bhavna_churidhar.jpg"


def sha256sum(filename):
    h  = hashlib.sha256()
    with open(filename, 'rb') as f:
        with mmap.mmap(f.fileno(), 0, prot=mmap.PROT_READ) as mm:
            h.update(mm)
    return h.hexdigest()

# XXX sending
def prepare_mail(type):

  msg = MIMEMultipart() 
  tonow = choice(to)
  msg['From'] = fromaddr 
  msg['To'] = tonow 

  if type == 'SPAM':
     sub = "Test spam from Gtube"
     body = '''

This is the GTUBE, the
	Generic
	Test for
	Unsolicited
	Bulk
	Email

XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X
  XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X

'''
     h = hashlib.sha256()
     h.update(body.encode('utf-8'))
     h = h.hexdigest()
     print("Sent mail with spam ")
  elif type == 'VIRUS':
     sub = "Test Eicar virus "
     body = r'X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'
     h = hashlib.sha256()
     h.update(body.encode('utf-8'))
     h = h.hexdigest()
     print("Sent mail with virus ")
  elif type == 'BANATT':
     sub = "Test for banned attachment"
     body = 'Plain vanilla mail with attachment'
     p = MIMEBase('application', 'octet-stream') 
     attachment = open(attach, "rb") 
     filename = os.path.basename(attach)
     h= sha256sum(attach)
     p.set_payload((attachment).read()) 
     encoders.encode_base64(p) 
     p.add_header('Content-Disposition', "attachment; filename= %s" %
     filename) 
     msg.attach(p) 
     print("Sent mail with attachment having hash ::", h)

  msg['Subject'] = sub
  msg.attach(MIMEText(body, 'plain')) 
  return(msg, h, tonow)


def despatch(msg, to):
  s = smtplib.SMTP('spamcheetah.com', 587) 
  s.connect('spamcheetah.com', 587)
  s.ehlo()
  s.starttls()
  s.ehlo()
  s.login('girish', 'mango');
  text = msg.as_string() 
  s.sendmail(fromaddr, to, text) 
  s.quit() 

def prepare_mail_simple():

  msg = MIMEMultipart() 
  
  msg['From'] = fromaddr 
  msg['To'] = toaddr 
  sub = "Test for speed"
  msg['Subject'] = sub
  msg.attach(MIMEText(body, 'plain')) 
  h = hashlib.sha256()
  h.update(body.encode('utf-8'))
  h = h.hexdigest()
  return(msg, h, toaddr)


# XXX receiving
def init_check_mail():
  server = IMAPClient('mta.spamcheetah.com', use_uid=True)
  server.login('girish', 'foo')
  select_info = server.select_folder('INBOX')
  print('%d messages in INBOX' % select_info[b'EXISTS'])
  messages = server.search()
  print("%d mails " % len(messages))
  return(server, messages)
#messages = server.search(['FROM', 'best-friend@domain.com'])

def list_messages(server):
   messages = server.search()
   for msgid, data in server.fetch(messages, ['ENVELOPE']).items():
    envelope = data[b'ENVELOPE']
    print('ID #%d: "%s" received %s' % (msgid,
    envelope.subject.decode(), envelope.date))
    h = hashlib.sha256()
    h.update(envelope.body)
    h = h.hexdigest()
   return(h)


def extract_attachments():
  with MailBox('mta.spamcheetah.com').login(
         'girish', 'foo', 'INBOX') as mailbox:
    hash = None
    for message in mailbox.fetch():
        for att in message.attachments:  # list: [Attachment objects]
            with open("/tmp/" + att.filename, "wb") as f:
              f.write(bytearray(att.payload))
            f.close()
            hash = sha256sum(att.filename)
            os.unlink("/tmp/" + att.filename)
  return(hash)


def do_sending(type):
  if type == "BANATT" or type == "VIRUS" or type == "SPAM":
    msg, h,to = prepare_mail(type)
  else:
    msg, h, to = prepare_mail_simple()
  despatch(msg, to)
  return(h)

def do_receiving(type):
 server, msg = init_check_mail()
 h = list_messages(server)
 if type == "attach":
   h = extract_attachments()
   
 return(server,msg, h)
 
def del_mails(server, messages):
  ret = server.delete_messages(messages)
  print("Delete messages ::" , ret)
  server.expunge()
  server.logout()

#@pytest.mark.repeat(5)
@pytest.mark.skip()
def test_mix(capsys):
 type = choice(["BANATT", "VIRUS", "SPAM"])
 print("Sending::" + type)
 hsend =  do_sending(type)
 return
 time.sleep(3)
 serv, msg, hrecv = do_receiving("attach")
 out,err = capsys.readouterr()
 sys.stdout.write(out)
 sys.stderr.write(err)
 # Cleanup
 del_mails(serv, msg)
 assert hsend == hrecv

##@pytest.mark.repeat(5)
@pytest.mark.skip()
def test_simple(capsys):
 hsend =  do_sending("simple")
 return
 time.sleep(3)
 serv, msg, hrecv = do_receiving("simple")
 out,err = capsys.readouterr()
 sys.stdout.write(out)
 sys.stderr.write(err)
 # Cleanup
 del_mails(serv, msg)
 assert hsend == hrecv

@pytest.mark.repeat(5)
#@pytest.mark.skip()
def test_min(capsys):
 hsend =  do_sending("simple")

