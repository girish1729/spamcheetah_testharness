import json
import pytest
import requests
import sys
from datetime import datetime
import psycopg2
from truth.truth import AssertThat

##################################################
# This is the forgot password test suite
##################################################

# XXX receiving
import sys
from imapclient import IMAPClient
import hashlib
import mmap
from imap_tools import MailBox


def dbOpen():
  con = psycopg2.connect(database="postgres", user="postgres",
  host="/var/run/postgresql")
  print("Database opened successfully")
  return(con)

def queryDB(con):
  cur = con.cursor()
  cur.execute('''select * from mails;''')
  rows = cur.fetchall()
  for row in rows:
      print(row)
  print("Table queried successfully")

def updateTable(con, upd):
  cur = con.cursor()
  cur.execute(upd)
  con.commit()
  print("Total updated rows:", cur.rowcount)

def del_mails(server, messages):
  server.delete_messages(messages)
  server.expunge()
  server.logout()


def deleteTable(con, table):
  cur = con.cursor()
  cur.execute(f"DELETE from {table};")
  con.commit()
  print("Total deleted rows:", cur.rowcount)

def get_req(stem, PARAMS=None):
  resp = requests.get("http://test.spamcheetah.com/cgi-bin/" + stem,
   params=PARAMS, auth=('access','dance'))
  return(resp)

# XXX receiving
def init_check_mail():

  server = IMAPClient('spamcheetah.zapto.org', use_uid=True)
  server.login('girish', 'panache1729')
  select_info = server.select_folder('INBOX')
  print('%d messages in INBOX' % select_info[b'EXISTS'])
  messages = server.search()
  print("%d mails " % len(messages))
  return(server, messages)
#messages = server.search(['FROM', 'best-friend@domain.com'])

def list_messages(server):
   messages = server.search()
   for msgid, data in server.fetch(messages, ['ENVELOPE']).items():
    envelope = data[b'ENVELOPE']
    sub = envelope.subject.decode()
    print('ID #%d: "%s" received %s' % (msgid,
    sub, envelope.date, ))
    assert sub == "Password reset from SpamCheetah"


# XXX start here

@pytest.fixture(autouse=True)
def fixture_function():
     assert True
     yield
     con = dbOpen()
     deleteTable(con, "password_change_requests")

def test_forgotpass_invoke(capsys):
   resp = get_req("forgotPassword", {"action":"send_forgotLink",
	'email': 'girish@test.spamcheetah.com'
   })
   print(resp.json())
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def login_newpass(id, capsys):
   resp = get_req("forgotPassword", {"action":"fetch_ident_fromLink",
	'idstring': id
   })
   print(resp.json())
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_checkmailer():
 server, msg = init_check_mail()
 list_messages(server)
 print("Got forgot password mailer ::", )
 # Cleanup
 del_mails(server, msg)

def test_forgotpass_link_activate(capsys):
   idstring = "";
   get_req("forgotPassword", {"action":"fetch_ident_fromLink",
"idstring": idstring})
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_link_expiry(capsys):
 server, msg = init_check_mail()
 list_messages(server)
 print("Got mail with attachment having hash ::", h)
 out,err = capsys.readouterr()
 sys.stdout.write(out)
 sys.stderr.write(err)
 return(h)
 

