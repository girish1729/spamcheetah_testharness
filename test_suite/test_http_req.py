import json
import pytest
import requests
import sys, os
from truth.truth import AssertThat

#######################################
#	Test all HTTP GET endpoints 
#######################################

#  XXX spamcheetah_config
# 	$verb = $q->param("action");
# 
# 	if($verb eq "license_check") {
# 		$jref = &license_check;
# 		print ($jref);
# 		exit;
# 	} elsif($verb eq "list_domains_mailids") {
# 		$jref = &list_domains_mailids;
# 		print ($jref);
# 		exit;
# 	} elsif($verb eq "activate_license") {
# 		$doms = $q->param('domains_allowed');
# 		$mailids = $q->param('mailids_allowed');
# 		$schash{'domains_allowed'} = $dom;
# 		$schash{'mailids_allowed'} = $mailids;
# 		sendcmd("ACTIVATE_LICENSE");
# 	} elsif($verb eq "blockedmime_del") {
# 		$mime = $q->param('mime');
# 		@{$schash{'blockedmime'}} = &del_item('mime', $mime, @{$schash{'blockedmime'}});
# 	} elsif($verb eq "blockedmime_add") {
# 		$mime = $q->param('mime');
# 		push @{$schash{"blockedmime"}},{'mime' => $mime};
# 	} elsif($verb eq "bannedsender_del") {
# 		$mailid = $q->param('mailid');
# 		@{$schash{'bannedsenderids'}} = &del_item('mailid', $mailid, @{$schash{'bannedsenderids'}});
# 	} elsif($verb eq "bannedsender_add") {
# 		$mailid = $q->param('mailid');
# 		push @{$schash{"bannedsenderids"}},{'mailid' => $mailid};
# 	} elsif($verb eq "bannedrecipient_del") {
# 		$mailid = $q->param('mailid');
# 		@{$schash{'bannedrecipids'}} = &del_item('mailid', $mailid, @{$schash{'bannedrecipids'}});
# 	} elsif($verb eq "bannedrecipient_add") {
# 		$mailid = $q->param('mailid');
# 		push @{$schash{"bannedrecipids"}},{'mailid' => $mailid};
# 	} elsif($verb eq "quaexcept_del") {
# 		$mailid = $q->param('mailid');
# 		@{$schash{'quaexcept'}} = &del_item('mailid', $mailid, @{$schash{'quaexcept'}});
# 	} elsif($verb eq "quaexcept_add") {
# 		$mailid = $q->param('mailid');
# 		push @{$schash{"quaexcept"}},{'mailid' => $mailid};
# 	} elsif($verb eq "discexcept_del") {
# 		$mailid = $q->param('mailid');
# 		@{$schash{'disclaimerexcept'}} = &del_item('mailid', $mailid, @{$schash{'disclaimerexcept'}});
# 	} elsif($verb eq "discexcept_add") {
# 		$mailid = $q->param('mailid');
# 		push @{$schash{"disclaimerexcept"}},{'mailid' => $mailid};
# 
# 	} elsif($verb eq "static_route_del") {
# 		$dest = $q->param('dest');
# 		@{$schash{'static_routes'}} = &del_item('destination', $dest, @{$schash{'static_routes'}});
# 	} elsif($verb eq "static_route_add") {
# 		$dest = $q->param('dest');
# 		$gateway = $q->param('gwy');
# 		push @{$schash{"static_routes"}},{'destination' => $dest, 'gateway' => $gateway};
# 	} elsif($verb eq "iface_alias_del") {
# 		$ipaddr = $q->param('ip');
# 		@{$schash{'iface_aliases'}} = &del_item('ip', $ipaddr, @{$schash{'iface_aliases'}});
# 	} elsif($verb eq "iface_alias_add") {
# 		$ipaddr = $q->param('ipaddr');
# 		$netmask = $q->param('mask');
# 		push @{$schash{"iface_aliases"}},{'ip' => $ipaddr, 'netmask' => $netmask};
# 	} elsif($verb eq 'change_ip') {
# 		$ipaddr = $q->param('ipaddr');
# 		$netmask = $q->param('mask');
# 		$intf_value = $q->param('netif');
# 		$gwy = $q->param('gwy');
# 		$line = join "\n", $intf_value, $ipaddr, 
# 			$netmask, $gwy;
# 		sendcmd("CHANGE_IP" . "\n" . $line);
# 	}
# } elsif($verb eq "mtatest") {
# 		$mtaip = $q->param('mtaip');
# 		my $sock = new IO::Socket::INET (
# 			PeerAddr => $mtaip,
# 			PeerPort => '25',
# 			Proto => 'tcp',
# 			Timeout => 15
# 			);
# 		syslog("info", "Connected to MTA");
# 		($greeting) = $sock->getline;	
# 		chomp($greeting);
# 		chop($greeting);
# 		syslog("info", "Read greeting ");
# 		close($sock);
# 	        print($json->encode({"greeting" => $greeting}));
# 		exit;
# 	} elsif($verb eq "clearqua") {
# 		$mailid = $q->param('mailid');
# 		sendcmd("CLEARQUA" . "\n". $mailid);
# 		syslog("info", "Cleared quarantine for $mailid");
# 	} elsif($verb eq "ourdomains") {
# 		$f = $q->param('filename');
# 		sendcmd("OURDOMAINS" . "\n". $f);
# 		syslog("info", "Got $f as domains file");
# 	} elsif($verb eq "showdomains") {
# 		my $proxy = IO::Socket::UNIX->new(
# 			Type      => SOCK_STREAM,
# 			Peer => $proxysock);
# 		print $proxy "LISTDOMAINS";
# 		@proxyvals = <$proxy>;
# 	        print($json->encode(\@proxyvals));
# 		exit;
# 	} elsif($verb eq "mtamap") {
# 		$f = $q->param('filename');
# 		sendcmd("MTAMAP" . "\n". $f);
# 		syslog("info", "Got $f as mtamap file");
# 		$schash{'mtaip'} = '127.0.0.1';
# 	} elsif($verb eq "showmapping") {
# 		open F, "/etc/mail/mtamap.txt";
# 		@maplines = <F>;
# 		@dommap = ();
# 		for $line (@maplines) {	
# 			chomp($line);
# 			next unless($line =~ /\w/);
# 			($dom, $ip) = split / /, $line;		
# 			$resp = &SMTPconn($ip);
# 			push @dommap,{ "ip" => $ip, 
# 				"dom" => $dom, "greet" => $resp};
# 		}
# 		print($json->encode(\@dommap));
# 		exit;
# 	}
# 	
# 
# 
#  XXX dbQuery
# 
# if($query eq 'mails') {
# 	@arr = SpamCheetahDBQuery::dbQuery("mails");
# 	PdfXLS::createpdfxlsx([@arr], "mails");
# 	print($json->canonical->pretty->encode(\@arr));
# 	syslog("info", "Fetching all  mails");
# } elsif($query eq "quamails") {
# 	@arr = SpamCheetahDBQuery::dbQuery("quamails");
# 	PdfXLS::createpdfxlsx([@arr], "quamails");
# 	syslog("info", "Fetching all quarantine mails");
# 	print($json->canonical->pretty->encode(\@arr));
# } elsif($query eq "geoip") {
# 	@arr = SpamCheetahDBQuery::dbQuery("geoip");
# 	print($json->canonical->pretty->encode(\@arr));
# } elsif($query eq "quamails_peruser") {
# 	$id = $q->param('toid');
# 	syslog("info", "Fetching per user quamail for $id");
# 	@arr = SpamCheetahDBQuery::dbQuery("quamails_peruser", $id);
# 	PdfXLS::createpdfxlsx([@arr], "quamails");
# 	print($json->canonical->pretty->encode(\@arr));
# } elsif($query eq "quamail_delete_mail") {
# 	$id = $q->param('id');
# 	syslog("info", "Deleting mail with serial $id");
# 	SpamCheetahDBQuery::dbQuery("quamail_delete_mail", $id);
# } elsif($query eq "fetch_toid_fromLink") {
# 	$id = $q->param('idstring');
# 	$linesref = SpamCheetahDBQuery::dbQuery("fetch_toid_fromLink", $id);
# 	syslog("info", "Fetching to_id from link for $id");
# 	print($json->canonical->pretty->encode($linesref));
# } elsif($query eq "quamail_deliver_mail") {
# 	$id = $q->param('id');
# 	syslog("info", "Delivering mail for $id");
# 	SpamCheetahDBQuery::dbQuery("quamail_deliver_mail", $id);
# }

def getvals(stem, PARAMS):
  resp = requests.get("http://test.spamcheetah.com/cgi-bin/" + stem,
   params=PARAMS, auth=('access','dance'))
  return(resp)
#######################################
# XXX 

# Arrange
# Act
# Assert
# Cleanup
#def teardown_function():

def test_spamconf_license_check(capsys):
   resp = getvals("spamcheetah_config", {"action":"license_check"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_list_domains_mailids(capsys):
   resp = getvals("spamcheetah_config", {"action":"list_domains_mailids"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_fwmode(capsys):
   resp = getvals("spamcheetah_config", {"action":"fwmode"})
   print(resp.json())
   json = resp.json()
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_spamconf_activate_license(capsys):
   resp = getvals("spamcheetah_config", {"action":"activate_license", 
	"domains_allowed": 100,
	"mailids_allowed": 300})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_blockedmime_del(capsys):
   resp = getvals("spamcheetah_config", {"action":"blockedmime_del",
		"mime": "wav"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_blockedmime_add(capsys):
   resp = getvals("spamcheetah_config", {"action":"blockedmime_add",
		"mime": "wav"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_bannedsender_del(capsys):
   resp = getvals("spamcheetah_config", {"action":"bannedsender_del",
		"mailid": "bhavana@yahoo.com"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_bannedsender_add(capsys):
   resp = getvals("spamcheetah_config", {"action":"bannedsender_add",
		"mailid": "bhavana@yahoo.com"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_bannedrecipient_del(capsys):
   resp = getvals("spamcheetah_config", {"action":"bannedrecipient_del",
		"mailid": "bhavana@gmail.com"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_bannedrecipient_add(capsys):
   resp = getvals("spamcheetah_config", {"action":"bannedrecipient_add",
		"mailid": "bhavana@gmail.com"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_quaexcept_del(capsys):
   resp = getvals("spamcheetah_config", {"action":"quaexcept_del",
		"mailid": "sneha@gmail.com"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_quaexcept_add(capsys):
   resp = getvals("spamcheetah_config", {"action":"quaexcept_add",
		"mailid": "thrisha@gmail.com"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_discexcept_del(capsys):
   resp = getvals("spamcheetah_config", {"action":"discexcept_del",
		"mailid": "bhavana@white.in"})
   print(resp.json())
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_discexcept_add(capsys):
   resp = getvals("spamcheetah_config", {"action":"discexcept_add",
		"mailid": "shriya@black.ca"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_static_route_del(capsys):
   resp = getvals("spamcheetah_config", {"action":"static_route_del",
		"dest": "192.158.1.2"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_static_route_add(capsys):
   resp = getvals("spamcheetah_config", {"action":"static_route_add",
		"dest": "192.158.1.2/100",
		"gwy": "19.15.1.2"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_iface_alias_del(capsys):
   resp = getvals("spamcheetah_config", {"action":"iface_alias_del",
		"ip": "192.158.1.2"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_iface_alias_add(capsys):
   resp = getvals("spamcheetah_config", {"action":"iface_alias_add",
		"ipaddr": "192.158.1.2",
         	"netif" : "vio0",
		"mask": "255.0.0.0"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

@pytest.mark.skip()
def test_spamconf_change_ip(capsys):
   resp = getvals("spamcheetah_config", {"action":"change_ip",
		"ipaddr": "192.158.1.2",
		"mask": "255.0.0.0",
		"netif": "vio0",
		"gwy": "12.0.0.1"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_spamconf_mtatest():
   resp = getvals("spamcheetah_config", {"action":"mtatest",
		"mtaip": "65.21.149.5"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200


def test_spamconf_clearqua():
   resp = getvals("spamcheetah_config", {"action":"clearqua",
		"mailid": "bhavana@yahoo.com"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_spamconf_showdomains():
   resp = getvals("spamcheetah_config", {"action":"showdomains"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200


def test_spamconf_showmapping():
   resp = getvals("spamcheetah_config", {"action":"showmapping"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200


def test_dbquery_mails(capsys):
   resp = getvals("dbQuery", {"q":"mails"})
   #print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_dbquery_quamails(capsys):
   resp = getvals("dbQuery", {"q":"quamails"})
   #print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_dbquery_geoip(capsys):
   resp = getvals("dbQuery", {"q":"geoip"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_dbquery_quamails_peruser(capsys):
   resp = getvals("dbQuery", {"q":"quamails_peruser", "toid": "girish@yahoo.in"})
   #print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_dbquery_quamail_delete_mail(capsys):
   resp = getvals("dbQuery", {"q":"quamails_delete_mail", "id": "girish@yahoo.in"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_dbquery_fetch_toid_fromLink(capsys):
   resp = getvals("dbQuery", {"q":"fetch_toid_fromLink", "idstring": "shasum"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_dbquery_quamail_deliver_mail(capsys):
   resp = getvals("dbQuery", {"q":"quamails_deliver_mail", "id": "girish@yahoo.in"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)
