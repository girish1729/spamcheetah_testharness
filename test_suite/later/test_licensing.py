
import json
import pytest
import sys
from datetime import datetime

##################################################
# This is the Licensing test suite
##################################################

def post_req(body):
  resp = requests.post("http://spamcheetah.zapto.org/cgi-bin/licheck",
   json=body, auth=('access','dance'))
  return(resp)


def test_buy_product(capsys):
#  purchase_license
#		$customer_name = $inpLic{customer_name};
#		$customer_email = $inpLic{customer_email};
#		$payment_id = $inpLic{payment_id};
#		$days = $inpLic{days};
#		$lic_type = $inpLic{license_type};
#		$product = $inpLic{product_name};
#		$domains = $inpLic{domains};
#		$mailids = $inpLic{mailids};
#
   body = {
            "verb": "purchase_license",
            "customer_name": "somecust",
            "customer_email": "some@yahoo.in",
            "payment_id": "axafdsdaf",
            "product": "SpamCheetah",
            "days": 180,
            "lic_type": "Trial",
            "domains": 40,
            "mailids": 800
   }
   resp = post_req(body)
   print(resp.json())

   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_activate(capsys):
#  activate
#		$customer_email = $inpLic{customer_email};
#		$customer_name = $inpLic{customer_name};
#		$product_key = $inpLic{product_key};
#		$product = $inpLic{product_name};
#
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_expire(capsys):
#  deactivate
#		$customer_email = $inpLic{"customer_email"};
#		$product = $inpLic{product_name};
#
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_reactivate(capsys):
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_exceed_license_time(capsys):
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_exceed_license_domain(capsys):
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_exceed_license_mailid(capsys):
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


#  check_license
#		$product_key = $inpLic{product_key};
#		$customer_email = $inpLic{"customer_email"};
#		$product = $inpLic{product_name};
def test_check_license(capsys):

   body = {
            "verb": "check_license",
            "customer_name": this.customer_name,
            "customer_email": this.customer_email,
            "product": "SpamCheetah",
            "product_key": this.product_key
   }
   resp = post_req(body)
   print(resp.json())
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)



#  dump_all
