
import json
import pytest
import sys
from datetime import datetime


##################################################
# This is the Clustering test suite
##################################################

def getvals(body):
  resp = requests.post("http://spamcheetah.hopto.org/cgi-bin/spamcheetah_config",
   json=body, auth=('access','dance'))
  return(resp)

def post_req(body):
  resp = requests.post("http://spamcheetah.hopto.org/cgi-bin/spamcheetah_config",
   json=body, auth=('access','dance'))
  return(resp)

# XX

# XXX Start
def test_cluster_config(capsys):
   confvals =  [
            'cluster_mode',
	    'cluster_ip',
	    'cluster_priority',
	    'clustering'
	];
   v, full = getvals(confvals)
   print(v)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)
   full["cluster_mode"] = "ACTIVE"
   full["cluster_ip"] = "2.3.4.5"
   full["cluster_priority"] = "Primary"
   full["clustering"] = True
   writeoutconf(full)


def test_active(capsys):
   confvals =  [ ];
   v = getvals(confvals)
   print(v)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_passive(capsys):
   confvals =  [ ];
   v = getvals(confvals)
   print(v)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


def test_sync(capsys):
   confvals =  [ ];
   v = getvals(confvals)
   print(v)
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)


