import json
import pytest
import requests
import sys
from datetime import datetime
from random import choice
from truth.truth import AssertThat

##################################################
# This is the greylisting test suite
##################################################
# 7 Greylisting
# 	7.1 Check delay of each mail
# 	7.2 How long does it take?
# 	7.3 Can tweaking values in conf make a difference?
# 	7.4 License expiry with/without greylisting
# 	7.5 How many hours delay does the user experience?
# 	7.6 How to manually Whitelist or Blacklist a sending MTA?

def getreq(PARMS):
  resp = requests.get("http://test.spamcheetah.com/cgi-bin/greylisting",
   params=PARMS, auth=('access','dance'))
  return(resp)

def postreq(body):
  resp = requests.post("http://test.spamcheetah.com/cgi-bin/greylisting",
   json=body, auth=('access','dance'))
  return(resp)

# Arrange
# Act
# Assert
# Cleanup
#def teardown_function():


# XXX Start

def test_get_grey_config():
   confvals =  [
		"greylisting",
                "greylist_passtime",
                "greylist_greyexp",
                "greylist_whiteexp",
                "greylist_blacklist_only",
                "greylist_stutter_delay"

	];
   resp = postreq({"verb":"fetch", "payload": confvals})
   print(resp.json())
   json = resp.json()
   assert 'greylisting' in resp.json()

def test_set_grey_config():
   passtime = choice(range(1,100))
   greyexp = choice(range(1,10))
   whitexp = choice(range(1,36))
   stut = choice(range(36,864))
   payload = {
   "greylisting": True,
   "greylist_passtime":  passtime,
   "greylist_greyexp":  greyexp,
   "greylist_whiteexp":  whitexp,
   "greylist_blacklist_only":  False,
   "greylist_stutter_delay":  stut
   };
   resp = postreq({"verb":"setGreylist", "payload": payload})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200


def whitelist_add():
   resp = getreq({"action":"whitelist_add", "ip": '1.2.3.4'})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_whitelist_show():
   resp = getreq({"action":"whitelist_show"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_whitelist_delete():
   resp = getreq({"action":"whitelist_delete", "ip": '1.2.3.4'})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_show_white():
   resp = getreq({"action":"show_white"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_show_grey():
   resp = getreq({"action":"show_grey"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_blacklist_add():
   resp = getreq({"action":"blacklist_add", "ip": '1.2.3.4'})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_blacklist_show():
   resp = getreq({"action":"blacklist_show"})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

def test_blacklist_delete():
   resp = getreq({"action":"blacklist_delete", "ip": '1.2.3.4'})
   print(resp.json())
   json = resp.json()
   assert json['code'] == 200

