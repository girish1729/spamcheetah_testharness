import pytest
import sys
import requests
import paramiko
import re
import fileinput
from datetime import datetime
from truth.truth import AssertThat

##################################################
# This is the Firewall test suite
##################################################

ip='45.32.250.237'
port=22
username='girish'
password='panache'

def getvals(body):
  resp = requests.post("http://test.spamcheetah.com/cgi-bin/spamcheetah_config",
   json=body, auth=('access','dance'))
  return(resp)

def post_req(body):
  resp = requests.post("http://test.spamcheetah.com/cgi-bin/spamcheetah_config",
   json=body, auth=('access','dance'))
  return(resp)

def grey_post_req(body):
  resp = requests.post("http://test.spamcheetah.com/cgi-bin/greylisting",
   json=body, auth=('access','dance'))
  return(resp)

def pf_req():
  resp = requests.get("http://test.spamcheetah.com/cgi-bin/maint",
   params={'action':'export_pf'}, auth=('access','dance'))
  return(resp)

def expire_req():
  resp = requests.get("http://test.spamcheetah.com/cgi-bin/maint",
   params={'action':'expire_license'}, auth=('access','dance'))
  return(resp)

def activate_req():
  resp = requests.get("http://test.spamcheetah.com/cgi-bin/spamcheetah_config",
   params={'action':'activate_license', 'domains_allowed': 100,
'mailids_allowed': 200}, auth=('access','dance'))
  return(resp)

# XXX 
# Start
def test_grey_config(capsys):
   payload = {
   "greylisting": True,
   "greylist_blacklist_only":  False,
   };
   resp = grey_post_req({"verb":"setGreylist", "payload": payload})
   print(resp.json())
   pf_req()
   pfconf = requests.get('http://test.spamcheetah.com/pf.conf')
   print(pfconf.text)

def test_blonly_config(capsys):
   payload = {
   "greylisting": True,
   "greylist_blacklist_only":  True,
   };
   resp = grey_post_req({"verb":"setGreylist", "payload": payload})
   print(resp.json())
   pf_req()
   pfconf = requests.get('http://test.spamcheetah.com/pf.conf')
   print(pfconf.text)

def test_normal_config(capsys):
   payload = {
   "greylisting": False,
   "greylist_blacklist_only":  True,
   };
   resp = grey_post_req({"verb":"setGreylist", "payload": payload})
   print(resp.json())
   pf_req()
   pfconf = requests.get('http://test.spamcheetah.com/pf.conf')
   print(pfconf.text)
 
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)

def test_license_expiry(capsys):
   expire_req()
   pf_req()
   pfconf = requests.get('http://test.spamcheetah.com/pf.conf')
   print(pfconf.text)

def test_license_reactivate(capsys):
   activate_req()
   pf_req()
   pfconf = requests.get('http://test.spamcheetah.com/pf.conf')
   print(pfconf.text)


def test_mta_down(capsys):
   cmd='/bin/activate_internal_mta' 
   ssh=paramiko.SSHClient()
   ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
   ssh.connect(ip,port,username,password)
   
   stdin,stdout,stderr=ssh.exec_command(cmd)
   outlines=stdout.readlines()
   resp=''.join(outlines)
   print(resp)
   pf_req()
   pfconf = requests.get('http://test.spamcheetah.com/pf.conf')
   print(pfconf.text)

def test_mta_up(capsys):
   out,err = capsys.readouterr()
   sys.stdout.write(out)
   sys.stderr.write(err)
   pf_req()
   pfconf = requests.get('http://test.spamcheetah.com/pf.conf')
   print(pfconf.text)

